﻿using Desktop_Exercise_3.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Desktop_Exercise_3
{
  class Program
  {
    static void Main(string[] args)
    {
      //inside this using statement, db is your database connection; the source of all of your queries
      //outside of the using statement, the database connection no longer exists and is cleaned up
      // Query1
      var myCustomer1 = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query1 = db.Customers.OrderBy(x => x.AccountNumber);
        myCustomer1 = Query1.ToList();
      }

      //path to the output file contained in the project
      var path1 = @"../../Query1.txt";
      using (var streamWrite = new StreamWriter(path1))
      {
        streamWrite.WriteLine("Total number of accounts ={0}", myCustomer1.Count);
        myCustomer1.ForEach(x => streamWrite.WriteLine("{0}", x.AccountNumber));

      }
      // Query2 using lambda notation 
      var myCustomer2 = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query2 = db.Customers.Include(x => x.Person)
          .Include(y => y.Store).Include(z => z.SalesOrderHeaders);
        //.Include(x => x.Person.FirstName); this is wrong
        myCustomer2 = Query2.ToList();
      }

      //path to the output file contained in the project
      var path2 = @"../../Query2.txt";
      using (var streamWrite2 = new StreamWriter(path2))
      {
        //streamWrite.WriteLine("Total number of accounts ={0}", myCustomer1.Count);
        //myCustomer2.ForEach(x => streamWrite.WriteLine("{0},{1}", x.CustomerID,x.Person.LastName));
        foreach (var p in myCustomer2)
        {
          //check if property is nullable if it is handle it in some fasshion
          streamWrite2.WriteLine("{0}, {1}, {2}, {3}, {4}",
            p.CustomerID,
            (p.Person != null) ? p.Person.LastName : "N/A",
            (p.Person != null) ? p.Person.FirstName : "N/A",
            p.AccountNumber,
            (p.Store != null) ? p.Store.Name : "N/A"
            );
          if (p.SalesOrderHeaders.Count > 0)
          {
            streamWrite2.WriteLine("------------------Sales----------------------");
            foreach (var x in p.SalesOrderHeaders)
            {
              var theSubtotal = x.SubTotal;
              streamWrite2.WriteLine("{0}, {1}, {2}, {3}", x.SubTotal, x.TaxAmt, x.Freight, x.TotalDue);
            }
          }
          else
          {
            streamWrite2.WriteLine("-------------No Sales -----------------");
          }
        }
      }
      // Query2 using linq notation
      var myCustomer2L = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query2L = (from c in db.Customers
                       join p in db.People on c.CustomerID equals p.BusinessEntityID into cp
                       from subp in cp.DefaultIfEmpty()
                       join s in db.Stores on c.CustomerID equals s.BusinessEntityID into cs
                       from subs in cs.DefaultIfEmpty()
                       join soh in db.SalesOrderHeaders on c.CustomerID equals soh.CustomerID into csoh
                       from subsoh in csoh.DefaultIfEmpty()
                       orderby subp.LastName
                       orderby subp.FirstName
                       select c)
                         .Include(x => x.Person)
                         .Include(x => x.Store)
                         .Include(x => x.SalesOrderHeaders);
        myCustomer2L = Query2L.ToList();
      }

      //path to the output file contained in the project
      var path2L = @"../../Query2L.txt";
      using (var streamWrite2L = new StreamWriter(path2L))
      {
        foreach (var c in myCustomer2L)
        {
          streamWrite2L.WriteLine("{0}, {1}, {2}, {3}, {4}",
            c.CustomerID,
            (c.Person != null) ? c.Person.LastName : "N/A",
            (c.Person != null) ? c.Person.FirstName : "N/A",
            c.AccountNumber,
            (c.Store != null) ? c.Store.Name : "N/A"
            );

        }
        //streamWrite.WriteLine("Total number of accounts ={0}", myCustomer1.Count);
        //myCustomer1.ForEach(x => streamWrite.WriteLine("{0}", x.AccountNumber));

      }
      // Query3
      var myCustomer3 = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query3 = db.Customers.Include(s=>s.Person).Where(x => x.Person.LastName=="Chapman")
          .Include(y => y.Store).Include(z => z.SalesOrderHeaders);
        
        myCustomer3 = Query3.ToList();
      }

      //path to the output file contained in the project
      var path3 = @"../../Query3.txt";
      using (var streamWrite3 = new StreamWriter(path3))
      {
        foreach (var c in myCustomer3)
        {
          streamWrite3.WriteLine("{0}, {1}, {2}, {3}, {4}",
            c.CustomerID,
            (c.Person != null) ? c.Person.LastName : "N/A",
            (c.Person != null) ? c.Person.FirstName : "N/A",
            c.AccountNumber,
            (c.Store != null) ? c.Store.Name : "N/A"
            );

        }
      }


      // Query4
      var myCustomer4 = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query4 = db.Customers.Include(s => s.Person)
          .Include(y => y.Store).Include(z => z.SalesOrderHeaders).Where(r=> r.SalesOrderHeaders.Sum( f=>f.TotalDue)> (decimal) 300000);

        myCustomer4 = Query4.ToList();
      }

      //path to the output file contained in the project
      var path4 = @"../../Query4.txt";
      using (var streamWrite4 = new StreamWriter(path4))
      {
        foreach (var c in myCustomer4)
        {
          streamWrite4.WriteLine("{0}, {1}, {2}, {3}, {4}, Total Sales={5}",
            c.CustomerID,
            (c.Person != null) ? c.Person.LastName : "N/A",
            (c.Person != null) ? c.Person.FirstName : "N/A",
            c.AccountNumber,
            (c.Store != null) ? c.Store.Name : "N/A",
            c.SalesOrderHeaders.Sum(d=>d.TotalDue)
            );
          if (c.SalesOrderHeaders.Count > 0)
          {
            streamWrite4.WriteLine("------------------Sales----------------------");
         
            foreach (var x in c.SalesOrderHeaders)
            {
              streamWrite4.WriteLine("{0}, {1}, {2}, {3}", x.SubTotal, x.TaxAmt, x.Freight, x.TotalDue);
            }
          }
         
        }
      }



      // Query5
      var myCustomer5 = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query5 = db.Customers.Include(s => s.Person)
          .Include(y => y.Store).Include(z => z.SalesOrderHeaders);

        myCustomer5 = Query5.ToList();
      }

      //path to the output file contained in the project
      var path5 = @"../../Query5.txt";
      using (var streamWrite5 = new StreamWriter(path5))
      {
        foreach (var c in myCustomer5)
        {
          streamWrite5.WriteLine("{0}, {1}, {2}, {3}, {4}, Total Sales={5}",
            c.CustomerID,
            (c.Person != null) ? c.Person.LastName : "N/A",
            (c.Person != null) ? c.Person.FirstName : "N/A",
            c.AccountNumber,
            (c.Store != null) ? c.Store.Name : "N/A",
            c.SalesOrderHeaders.Sum(d => d.TotalDue)
            );
          if (c.SalesOrderHeaders.Count > 0)
          {
            streamWrite5.WriteLine("------------------Sales----------------------");

            foreach (var x in c.SalesOrderHeaders)
            {
              streamWrite5.WriteLine("{0}, {1}, {2}, {3}, {4}", x.SubTotal, x.TaxAmt, x.Freight, x.TotalDue,(x.Freight)/(x.TotalDue));
            }
          }

        }
      }
      // Query6
      var myCustomer6 = new List<Customer>();
      using (var db = new AdventureWorks2014Entities())
      {
        var Query6 = db.Customers.Include(s => s.Person)
          .Include(y => y.Store).Include(z => z.SalesOrderHeaders).OrderByDescending((w=>w.SalesOrderHeaders.Sum((ff=>ff.Freight/ff.TotalDue))));

        myCustomer6 = Query6.ToList();
      }

      //path to the output file contained in the project
      var path6 = @"../../Query6.txt";
      using (var streamWrite6 = new StreamWriter(path6))
      {
        foreach (var c in myCustomer6)
        {
          streamWrite6.WriteLine("{0}, {1}, {2}, {3}, {4}, Total Sales={5}",
            c.CustomerID,
            (c.Person != null) ? c.Person.LastName : "N/A",
            (c.Person != null) ? c.Person.FirstName : "N/A",
            c.AccountNumber,
            (c.Store != null) ? c.Store.Name : "N/A",
            c.SalesOrderHeaders.Sum(d => d.TotalDue)
            );
          if (c.SalesOrderHeaders.Count > 0)
          {
            streamWrite6.WriteLine("------------------Sales----------------------");

            foreach (var x in c.SalesOrderHeaders)
            {
              streamWrite6.WriteLine("{0}, {1}, {2}, {3}, {4}", x.SubTotal, x.TaxAmt, x.Freight, x.TotalDue, (x.Freight) / (x.TotalDue));
            }
          }

        }
      }

    
    }
  }
}
